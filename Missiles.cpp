
#include "Missiles.h"

bool loadMissileSprite(MISSILE* missile){

	//Skapar upp en missilsprite

	D3DXCreateSprite(direct3DDevice, &missile->missileSprite->sprite);

	return true;
}

MISSILE* createMissile(missileType missileOwner){
	
	//Skapar en missil, kan vara b�de spelarens eller aliens
	//S�tter alla grundattribut som beh�vs, men framf�rallt s�tts active till falsk
	//En sprite skapas med upp med olika attribut beroende p� om det �r en spelarmissil 
	//eller om det �r aliens. Se Sprite.cpp f�r f�rklaring p� hur de attributen fungerar
	
	MISSILE* missile = new MISSILE();
	
	missile->active = false;

	missile->missileOwner = missileOwner;

	missile->missileSprite = new SPRITE();

	if (missile->missileOwner==PLAYERTYPE){
		missile->missileSprite = createSprite(16, 16, 34, 1548, 6, 0, 0, 0);
	}

	if (missile->missileOwner == ALIENTYPE){
		missile->missileSprite = createSprite(28, 32, 372, 1431, 0, 0, 0, 0);
	}

	missile->missileSprite->map = { missile->missileSprite->mapLeft, missile->missileSprite->mapTop, missile->missileSprite->mapLeft + missile->missileSprite->width, missile->missileSprite->mapTop + missile->missileSprite->height };

	if (!loadMissileSprite(missile))
		return NULL;

	return missile;
}

void destroyMissile(MISSILE* missile){

	//Avallokerar minne f�r missilen och dess sprite.

	destroySprite(missile->missileSprite);
	delete missile;

}

void animateMissile(MISSILE* missile){
	
	//Om missilen �r aktiv s� animeras den.
	//Den h�r g�ller bara spelarens skott d� aliens inte animeras
	//Varje g�ng animationstimern n�r ett visst v�rde s� byts animationsruta
	
	if (missile->active){
		missile->delay = GetTickCount() - missile->startTime;
		if (missile->delay > 150){
			missile->startTime = GetTickCount();
			missile->missileSprite->frame++;
			if (missile->missileSprite->frame == 3)
				missile->missileSprite->frame = 0;
		}
	}
}

void moveMissile(MISSILE* missile, int speed){
	
	//Flyttar missilen p� skr�men. Ned�t om det �r aliens (och med aktuell niv�s hastighet)
	//Om det �r spelarens missil s� flyttas den upp�t sex pixlar
	//Om aliens missil g�r utanf�r sk�rmen s� avaktiveras den
	//Om spelarens skott n�r de bl� boxarna s� avaktiveras den
	
	int movement;
	
	if (missile->missileOwner == PLAYERTYPE){
		movement = -6;
	}
	else
		movement = speed;

	if (missile->active){
		missile->missileSprite->y = missile->missileSprite->y + movement;

		if (missile->missileSprite->y < 128 || missile->missileSprite->y >1200)
			missile->active = false;
	}
}

void drawMissile(MISSILE* missile){
	
	//Ritar ut spriten, om den �r aktiv, och aktuell animationsruta p� den spritens x- och y-kordinationer
	
	if (missile->active){
		missile->missileSprite->sprite->Begin(D3DXSPRITE_ALPHABLEND);
			D3DXVECTOR3 missilePosition(missile->missileSprite->x, missile->missileSprite->y, 0);
			RECT missileanimiation = { 0 + (missile->missileSprite->frame * 16), 0, 16 + (missile->missileSprite->frame * 16), 16 };
			missile->missileSprite->map = { missile->missileSprite->mapLeft + ((missile->missileSprite->spacing + missile->missileSprite->width)*missile->missileSprite->frame), missile->missileSprite->mapTop, missile->missileSprite->mapLeft + missile->missileSprite->width + ((missile->missileSprite->spacing + missile->missileSprite->width)*missile->missileSprite->frame), missile->missileSprite->mapTop + missile->missileSprite->height };
			missile->missileSprite->sprite->Draw(spriteSheet, &missile->missileSprite->map, NULL, &missilePosition, D3DCOLOR_XRGB(255, 255, 255));
		missile->missileSprite->sprite->End();
	}
}

void activateMissile(MISSILE* missile, float xPos){
	
	//Aktiverar skottet och startar timern f�r animering
	
	missile->active = true;
	missile->missileSprite->x = xPos; //+23 f�r att f� skotten i mitten av spriten
	missile->missileSprite->y = 1000;
	missile->startTime = GetTickCount();
}