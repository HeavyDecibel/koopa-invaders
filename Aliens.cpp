#include "aliens.h"

bool loadAlienSprite(ALIEN* alien){
	
	//Skapar en sprite f�r alien

	D3DXCreateSprite(direct3DDevice, &alien->sprite->sprite);

	return true;
}

ALIEN* createAlien(alienPosition alienPosition, int spacing, int destination, HWND window){
	
	//Initerar aliens. H�r s�tts position, spriten skapas, animationsrutor och timrar
	
	ALIEN* alien = new ALIEN();
	alien->position = alienPosition;
	alien->destroyed = false;
	alien->explode = false;
	alien->explosionFrame = 0;

	alien->destination = destination;
	alien->startAnimationTime = GetTickCount();

	alien->sprite = new SPRITE();

	//Aliens kan vara i raderna top, middle eller bottom och f�r d� olika startv�rden
	//V�rdet f�r spacing �r hur l�ngt det �r mellan sprites i bildfilen med sprites. Det h�r eftersom det 
	//�r olika l�ngt mellan varje sprite.
	
	if (alien->position == TOP){
		
		alien->sprite = createSprite(32, 48, 743, 857, 28, 400 + spacing * 80, 200, 0);

	}

	if (alien->position == MIDDLE){
		
		alien->sprite = createSprite(31, 30, 339, 1550, 13, 200 + spacing * 80, 300, 0);

	}

	if (alien->position == BOTTOM){
		
		alien->sprite = createSprite(32, 48, 743, 918, 28, 700 + spacing * 80, 400, 0);
		
	}

	//Var i bildfilen spriten finns
	alien->sprite->map = { alien->sprite->mapLeft, alien->sprite->mapTop, alien->sprite->mapLeft + alien->sprite->width, alien->sprite->mapTop + alien->sprite->height }; //Var i bildfilen spriten finns lokaliserad
	

	if (!loadAlienSprite(alien))
		return NULL;

	return alien;
}

void destroyAlien(ALIEN* alien){
	
	//Tar bort pekaren f�r alien och dess sprite
	
	destroySprite(alien->sprite);
	delete alien;
}

void drawAlien(ALIEN* alien){
	
	//Ritar ut alien p� sk�rmen
	//Det finns tv� olika typer av sprites som ritas ut, antingen en f�r en levande alien 
	//eller en f�r en exploderande alien. Det �r samma sprite-objekt som anv�nds och samma texture
	//men de finns p� olika platser i bildfilen och animeras med olika antal bildrutor
	
	alien->sprite->sprite->Begin(D3DXSPRITE_ALPHABLEND);
		D3DXVECTOR3 alienPosition(alien->sprite->x, alien->sprite->y, 0);

		if (!alien->destroyed){
			if (!alien->explode){
				alien->sprite->map = { alien->sprite->mapLeft + ((alien->sprite->spacing+alien->sprite->width)*alien->sprite->frame), alien->sprite->mapTop, alien->sprite->mapLeft + alien->sprite->width + ((alien->sprite->spacing+alien->sprite->width)*alien->sprite->frame), alien->sprite->mapTop + alien->sprite->height };
				alien->sprite->sprite->Draw(spriteSheet, &alien->sprite->map, NULL, &alienPosition, D3DCOLOR_XRGB(255, 255, 255));
			}
		}

		
		if (alien->explode){
			alien->sprite->map = { 120+(alien->explosionFrame*33), 1542, 120 + 33+(alien->explosionFrame*33), 1542 + 33 };
			alien->sprite->sprite->Draw(spriteSheet, &alien->sprite->map, NULL, &alienPosition, D3DCOLOR_XRGB(255, 255, 255));
		}
		
	alien->sprite->sprite->End();
}

void moveAlien(ALIEN* alien, int distance){

	//Flyttar alien ett visst antal steg i sidled
	
	alien->sprite->x += distance;

}


void animateAlien(ALIEN* alien){
	
	//Animerar aliens sprite. Som sagt finns det tv� olika animationer, en f�r explosioner (som �r fyra rutor) och en f�r
	//flaxande vingar/fenor som �r tv� rutor. De animeras med olika hastighet (det som �r explosionDelayTime).
	//S� fort timern n�r en utsatt tid s� r�knas rutorna upp och n�sta ruta ska visas p� sk�rmen. N�r en alien har exploderat klart
	//s� s�tts explodeflaggan till false f�r framtida anv�ndning

	if (alien->explode){
		alien->explosionDelayTime = GetTickCount() - alien->startExplosionTime;
		if (alien->explosionDelayTime > 50){
			alien->startExplosionTime = GetTickCount();
			alien->explosionFrame++;
			if (alien->explosionFrame == 3){
				alien->explode = false;
			}
		}
	}

	if (!alien->destroyed){
		if (!alien->explode){
			alien->animationTimeDelay = GetTickCount() - alien->startAnimationTime;
			if (alien->animationTimeDelay > 150){
				if (alien->sprite->frame == 0)
					alien->sprite->frame = 1;
				else
					alien->sprite->frame = 0;
				alien->startAnimationTime = GetTickCount();
			}
		}
	}
}