#include "Sprites.h"

//H�r finns variablar f�r ett g�ng olika sprites som anv�nds i spelet
//Anledningen till varf�r jag valt att ha med sprites f�r en del texter ist�llet f�r fonter
//�r helt enkelt att jag ville l�ra mig hur det fungerade. I kommande fall hade nog spriteanv�ndningen
//varit lite mer genomt�nkt

LPD3DXSPRITE pauseSprite;
LPDIRECT3DTEXTURE9 pauseTexture=NULL;
LPDIRECT3DTEXTURE9 spriteSheet = NULL; 

LPD3DXSPRITE levelCompleteSprite;
LPDIRECT3DTEXTURE9 levelCompleteTexture = NULL;

LPD3DXSPRITE lifeSprite;
LPDIRECT3DTEXTURE9 lifeTexture = NULL;

LPD3DXSPRITE gameOverSprite;
LPDIRECT3DTEXTURE9 gameOverTexture=NULL;

LPD3DXSPRITE blueBlockSprite;

static const int CLOUDS = 5;
SPRITE* cloudSprite[CLOUDS];

LPDIRECT3DSURFACE9 topSurface;
LPDIRECT3DSURFACE9 splashScreen;

//H�r nedan kommer en trave funktioner som g�r precis samma sak fast f�r olika sprites
//Jag testade att g�ra en funktion som ladda sprites med argument men fick det inte att fungera s� 
//som jag ville s� jag st�r fast vid den h�r l�sningen f�r det h�r projektet �ven om jag
//ogillar redundant kod.

//Kommenteringen h�r g�ller �ven likanande funktioner i Player.cpp, Missiles.cpp och Alien.cpp

bool loadPauseSprite(HWND window){
	
	pauseTexture = loadTexture("graphics/pause.tga", (255, 0, 255));  //<-Laddar en texturfil och s�tter rosa som transparent f�rg
	if (!pauseTexture) 
	{
		MessageBox(window, "Could not load spritefiles", "Error", MB_ICONERROR);
		return false; //<-Blir n�got fel s� skickas ett felmeddelande ut och falskt returneras
	}

	D3DXCreateSprite(direct3DDevice, &pauseSprite); //<-Annars s� skapas spriten upp och �r redo att anv�ndas

	return true;
}

bool loadLevelCompleteSprite(HWND window){

	levelCompleteTexture = loadTexture("graphics/LevelComplete.tga", (255, 0, 255));
	if (!levelCompleteTexture)
	{
		MessageBox(window, "Could not load spritefiles", "Error", MB_ICONERROR);
		return false;
	}

	D3DXCreateSprite(direct3DDevice, &levelCompleteSprite);

	return true;

}


bool loadLifeSprite(HWND window){

	lifeTexture = loadTexture("graphics/life.png", (255, 0, 255));
	if (!lifeTexture)
	{
		MessageBox(window, "Could not load spritefiles", "Error", MB_ICONERROR);
		return false;
	}

	D3DXCreateSprite(direct3DDevice, &lifeSprite);

	return true;

}

bool loadGameOverSprite(HWND window){

	gameOverTexture = loadTexture("graphics/GameOver.png", (255, 0, 255));
	if (!gameOverTexture)
	{
		MessageBox(window, "Could not load spritefiles", "Error", MB_ICONERROR);
		return false;
	}

	D3DXCreateSprite(direct3DDevice, &gameOverSprite);

	return true;

}

bool loadBlueBlockSprite(){
	
	//Samma som ovan, men h�r beh�ver inte texturen laddas d� den redan finns i nedanst�ende funktion
	
	D3DXCreateSprite(direct3DDevice, &blueBlockSprite);

	return true;
}

void loadCloudSprite(SPRITE* cloudSprite){

	D3DXCreateSprite(direct3DDevice, &cloudSprite->sprite);

}



bool loadSpriteSheet(HWND window){

	//H�r laddas texturen in f�r majoriteten av sprites, det �r en fil med en upps�ttnings sprites som anv�nds

	spriteSheet = loadTexture("graphics/spritesheet.png", (255, 0, 255));
	if (!spriteSheet){
		MessageBox(window, "Could not load spritesheet", "Error", MB_ICONERROR);
		return false;
	}

	return true;
}

void createClouds(){

	//Molnsprites f�r bakgrund
	//x- och y-position �r slumpm�ssig i b�rjan f�r att inte alla ska m�las ut p� samma plats

	for (int i = 0; i < CLOUDS; i++){
		cloudSprite[i] = createSprite(64, 52, 621, 38, 0, 0, 0, 0);
		cloudSprite[i]->x = rand() % screenWidth;
		cloudSprite[i]->y = rand() % screenHeight;
		cloudSprite[i]->map = { cloudSprite[i]->mapLeft, cloudSprite[i]->mapTop, cloudSprite[i]->mapLeft + cloudSprite[i]->width, cloudSprite[i]->mapTop + cloudSprite[i]->height };
		loadCloudSprite(cloudSprite[i]);
	}

}

bool loadSprites(HWND window){
	
	//Laddar in sprites

	if (!loadPauseSprite(window))
		return false;

	if (!loadSpriteSheet(window))
		return false;

	if (!loadLevelCompleteSprite(window)){
		return false;
	}

	if (!loadGameOverSprite(window)){
		return false;
	}

	if (!loadBlueBlockSprite())
		return false;

	if (!loadLifeSprite(window))
		return false;

	createClouds(); //<-Skapar upp n�gra moln f�r bakgrunden

	//Skapar �ven en yta med svart f�rg fr�n en svart bildfil samt en yta f�r splashsk�rmen
	topSurface = loadSurface("graphics/BlackBackground.png");
	if (!topSurface){
		MessageBox(window, "Error loading surfacefile", "ERROR", 0);
		return false;
	}
	
	splashScreen = loadSurface("graphics/splashscreen.png");
	if (!splashScreen){
		MessageBox(window, "Error loading surfacefile", "ERROR", 0);
		return false;
	}
	
	return true;
}




void drawBlueBlocks(int screenWidth){
	//Ritar ut bl�a brickor �verst p� skr�men, de anv�nds som avgr�nsare f�r att visa po�ng, niv� och liv
	//Brickorna �r en och samma sprite som ritas ut flera g�nger.
	//Funktionen ritar ut 4 horistontella rader och 50 vertikala rader

	int columnsOfBlocks = screenWidth / 32;
	RECT blockPositionInSpriteSheet = { 275, 29, 275 + 32, 29 + 32 };

	//Horistonell rad
	for (int i = 0; i <= columnsOfBlocks; i++){
		D3DXVECTOR3 blockPosition(0 + (i * 32), 97, 0);
		blueBlockSprite->Begin(D3DXSPRITE_ALPHABLEND);
			blueBlockSprite->Draw(spriteSheet, &blockPositionInSpriteSheet, NULL, &blockPosition, D3DCOLOR_XRGB(255, 255, 255));
		blueBlockSprite->End();
	}

	int startCol;

	//Vertikala pelare

	for (int noCols = 0; noCols <= 3; noCols++){
		if (noCols == 0)
			startCol = 0; //<-V�rde som s�tts f�r var pelaren ska ritas ut i x-ledd
		if (noCols == 1)
			startCol = 480;
		if (noCols == 2)
			startCol = 960+32;
		if (noCols == 3)
			startCol = 1440+64;

		for (int i = 0; i <= 2; i++){
			for (int j = 0; j <= 2; j++){
				D3DXVECTOR3 blockPosition(startCol + (j * 32), 0 + (i * 32), 0);
				blueBlockSprite->Begin(D3DXSPRITE_ALPHABLEND);
				blueBlockSprite->Draw(spriteSheet, &blockPositionInSpriteSheet, NULL, &blockPosition, D3DCOLOR_XRGB(255, 255, 255));
				blueBlockSprite->End();
			}
		}
	}
}

void destroySprite(SPRITE* sprite){
	
	//Avallokerar minne f�r spriten
	
	delete sprite;
}

SPRITE* createSprite(int width, int height, int mapLeft, int mapTop, int spacing, float x, float y, int frame){
	
	//Spritens "konstruktor". S�tter bredd, h�jd, vilka x- och y-koordinater den har i filen, hur l�ngt det �r mellan
	//olika sprites i bildfilen, x- och y-kordinater p� sk�rmen samt animationsruta

	SPRITE* sprite = new SPRITE();

	sprite->width = width;
	sprite->height = height;
	sprite->mapLeft = mapLeft;
	sprite->mapTop = mapTop;
	sprite->spacing = spacing;
	sprite->x = x;
	sprite ->y = y;
	sprite->frame = frame;
	
	return sprite;
}

void moveClouds(){
	//Funktion som flytar moln p� bakgrunden. Om molnen trillar utanf�r sk�rmen s� slumpas en ny x-position ut 
	//och sedan s� flyttas de vidare ner�t

	for (int i = 0; i < CLOUDS; i++)
	{
		if (cloudSprite[i]->y > 1200){
			cloudSprite[i]->x = rand() % screenWidth;
			cloudSprite[i]->y = 0;
		}
		cloudSprite[i]->y += 1;
	}

}

void drawClouds(){

	//Ritar ut molnen i bakgrunden p� sk�rmen
		
	for (size_t i = 0; i < CLOUDS; i++)
	{
		cloudSprite[i]->sprite->Begin(D3DXSPRITE_ALPHABLEND);

		D3DXVECTOR3 cloudPosition(cloudSprite[i]->x, cloudSprite[i]->y, 0);
		cloudSprite[i]->sprite->Draw(spriteSheet, &cloudSprite[i]->map, NULL, &cloudPosition, D3DCOLOR_XRGB(255, 255, 255));

		cloudSprite[i]->sprite->End();
	}
}

void destroyClouds(){
	
	//Avallokerar minne f�r bakgrundsspriterna

	for (int i = 0; i < CLOUDS; i++){
		delete cloudSprite[i];
	}
	
	
}