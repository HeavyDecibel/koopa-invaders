
#ifndef __MISSILES_H_INCLUDED__
#define __MISSILES_H_INCLUDED__

#include "Direct3DGraphics.h"
#include "Sprites.h"

//Enum f�r att bed�mma om det �r en alien eller spelare som �ger skottet
extern enum missileType { PLAYERTYPE, ALIENTYPE };

struct MISSILE{

	int startTime, delay; //Timer f�r anmation
	bool active; //Om skottet �r aktivt (p� sk�rmen) eller ej

	missileType missileOwner; //Vem �garen �r

	SPRITE* missileSprite; //Spriten
};

MISSILE* createMissile(missileType);
void destroyMissile(MISSILE* missile);
void animateMissile(MISSILE* missile);
void moveMissile(MISSILE* missile, int speed);
void drawMissile(MISSILE* missile);
void activateMissile(MISSILE* missile, float xPos);

#endif