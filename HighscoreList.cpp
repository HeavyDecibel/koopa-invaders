#include "HighscoreList.h"

static const int MAXSCORES = 10; //Det f�r vara h�gst tio element i highscorelistan
HIGHSCOREENTRY* highscoreRow[MAXSCORES];


HIGHSCOREENTRY* createHighscoreRow(int rowNo){

	//Skapar upp ett element i highscorelistan. 

	HIGHSCOREENTRY* highscoreEntry = new HIGHSCOREENTRY();

	//L�ser in element fr�n vald rad i highscore.txt-filen
	readHighscoreFromFile(highscoreEntry, rowNo);

	return highscoreEntry;
}

HIGHSCOREENTRY* createHighscoreRow(){

	//Samma som ovan fast parameterl�s och som inte tittar i highscorefilen

	HIGHSCOREENTRY* highscoreEntry = new HIGHSCOREENTRY();

	highscoreEntry->score = 0;
	highscoreEntry->level = 0;

	return highscoreEntry;
}

void sortHighscore(){
	
	//Funktion som sorterar listan med enkel bubbelsortering

	bool sortingComplete = false;
	HIGHSCOREENTRY* tmpHighscoreRow;
	tmpHighscoreRow = createHighscoreRow(); //Anv�nds f�r att byta plats

	while (!sortingComplete){ //Loop som k�rs s� l�nge som listan inte �r f�rdigsorterad
		sortingComplete = true;
		for (int i = 0; i < MAXSCORES - 1; i++){ //Loop som itererar igenom alla namn och po�ng
			if (highscoreRow[i]->score<highscoreRow[i + 1]->score){ //J�mf�r aktuellt v�rde med n�sta v�rde och ser om det �r h�gre
				tmpHighscoreRow = highscoreRow[i];
				highscoreRow[i] = highscoreRow[i + 1];
				highscoreRow[i + 1] = tmpHighscoreRow;   //Om v�rdet �r h�gre s� byt plats p� v�rderna och sortera om
				sortingComplete = false;
			}
		}
	}

	//delete tmpHighscoreRow; //Det h�r orsakar ett ohanterat undantag. Jag f�r ingen mer information �n s� av Visual Studio.
	//Samtidigt s� �r det en lokal pekare s� jag vet inte om den beh�ver tas bort? Anv�nder jag delete F�RE while-loopen s� f�r jag inget fel
}

void readHighscoreFromFile(HIGHSCOREENTRY* highscoreEntry, int rowNo){
	
	//L�ser rader fr�n highscore.txt och l�gger �ver inneh�llet i ett element i highscorearrayen
	
	ifstream file("highscore.txt");

	char nextChar;
	string chars="";

	int col = 0;
	int row = 0;

	string output;

	//L�ser tecken-f�r-tecken. Filen �r enligt strukturen PO�NG NIV�.
	//S� den letar efter mellan slag, innan dess s� sl�r den ihop allt den hittar.
	//Om loopen st�ter p� ett mellanslag s� g�r den om det den hittat innan till en integer
	//och beroende p� om det �r po�ng- eller niv�kolumnen s� lastas det �ver till highscoreEntry

	if (file.is_open()){
		while (file.get(nextChar)){

			if (nextChar != ' ' || !nextChar != '\n'){
				chars = chars + nextChar;
			}

			if (nextChar == ' ' || nextChar=='\n'){
				if (row == rowNo){
					if (col == 0){
						highscoreEntry->score = atoi(chars.c_str());
					}
					if (col == 1){
						highscoreEntry->level = atoi(chars.c_str());
					}
				}
				chars = "";
				col++;
			}

			if (nextChar == '\n'){
				col = 0;
				row++;
				chars = "";
			}
		}
	}
}

void drawHighscoreScreen(){

	//Ritar ut de tio highscoreraderna p� sk�rmen med hj�lp av scoreFonten
	//Varje ny iteration l�gger p� ett litet mellanrum s� att de inte skrivs ut p� samma position
	
	RECT highscorePos = { (screenWidth/2-150), 200, 1200, 300 };
	string highScoreText = "";
	
	for (int i = 0; i < MAXSCORES; i++){
		highscorePos.top = highscorePos.top + (i + 50);
		highscorePos.bottom = highscorePos.bottom + (i + 50);
		highScoreText = to_string(i+1)+". " + to_string(highscoreRow[i]->score) + " (Level: " + to_string(highscoreRow[i]->level)+")";
		direct3DScoreFont->DrawTextA(NULL, highScoreText.c_str(), -1, &highscorePos, DT_LEFT, D3DCOLOR_XRGB(238, 20, 20));
	}
}



void setUpHighscoreList(){
	
	//Skapar upp en array med tio highscoreplatser
	//Avslutar med att sortera arrayen

	for (int i = 0; i < MAXSCORES; i++){
		highscoreRow[i] = new HIGHSCOREENTRY();
		highscoreRow[i] = createHighscoreRow(i);
	}

	sortHighscore();
}

void writeHighscoreToFile(){

	//Itererar igenom arreyn med highscores och skriver �ver highsore.txt

	ofstream highscorefile;
	highscorefile.open("highscore.txt");
	
	string text;
	for (int i = 0; i < MAXSCORES; i++){
		text = to_string(highscoreRow[i]->score) + " " + to_string(highscoreRow[i]->level)+"\n";
		highscorefile << text.c_str();
	}
	highscorefile.close();

}


void destroyHighscoreEntry(HIGHSCOREENTRY* highscoreEntry){
	
	//Avvallokerar minne f�r highscorepositionen
	
	delete highscoreEntry;
}

void exitHighscoreList(){
	
	//N�r highscorelistan avslutas s� skrivs den f�rst till fil
	//Och sedan tas alla pekare bort

	writeHighscoreToFile();

	for (int i = 0; i < MAXSCORES; i++){
		destroyHighscoreEntry(highscoreRow[i]);
	}
}

bool newHighScore(int score, int level){

	//Kontrollerar om po�ngen f�r den inmatade anv�ndaren �r h�gre �n det l�gsta v�rdet
	//i highscorelistan. Om det �r s� d� �r f�r namnet och po�ngen vara med.
	setUpHighscoreList();
	sortHighscore();

	if (score > highscoreRow[MAXSCORES - 1]->score){

		highscoreRow[MAXSCORES - 1]->level = level;
		highscoreRow[MAXSCORES - 1]->score = score;

		writeHighscoreToFile();
		exitHighscoreList();

		return true;

	}

	exitHighscoreList();
	return false;

}



