
#ifndef __GAMEENGINE_H_INCLUDED__
#define __GAMEEGINE_H_INCLUDED__

//GameEngine �r huvudfilen som h�ller ihop spelet, den inneh�ller i stort sett alla andra filer p� ett eller annat s�tt

#include <Windows.h>
#include "Direct3DGraphics.h"
#include "DirectInput.h"
#include "Direct3DFonts.h"
#include "Sprites.h"
#include "Player.h"
#include "Aliens.h"
#include "Missiles.h"
#include "level.h"
#include "DirectSound.h"
#include "Menu.h"
#include "HighscoreList.h"
#include "Sound.h"


extern int screenWidth;
extern int screenHeight;

extern bool gameOver;
extern bool gameRunning;

bool gameInit(HWND window);
void gameLoop(HWND window);
void gameEnd();

#endif