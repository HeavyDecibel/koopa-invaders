#include "GameEngine.h"
#include <string>

using namespace std;

//**********MENY**********
MENUELEMENT* newGameButton;
MENUELEMENT* resumeGameButton;
MENUELEMENT* highscoreButton;
MENUELEMENT* exitButton;
MOUSEPOINTER* mousePointer;
//**********MENY**********

//**********SK�RMSTORLEK********
int screenWidth = 1600;
int screenHeight = 1200;
//**********SK�RMSTORLEK********

//*********STYRPARAMETRAR**************************
bool gameOver;
bool gamePaused;
bool levelComplete;
bool splashscreenShowed;
bool menuVisible;
bool gameRunning = true;
bool highscoreVisible;
bool splashScreenVisible;
//*********STYRPARAMETRAR**************************

//***************VARIABLAR F�R SPELOBJEKT**********************
static const int PLAYERMISSILES=4;
static const int ALIENS = 30;
static const int ALIENMISSILES = 7;

PLAYER* player;
MISSILE* playerMissile[PLAYERMISSILES];
ALIEN* alien[ALIENS];
MISSILE* alienMissile[ALIENMISSILES]; //<-7 fiendeskott �r max som kan anv�ndas
//Egentligen ville jag skapa upp dessa helt dynamiskt, men jag k�nner att mina C++-kunskaper �r
//f�r svaga f�r det just nu. Enligt teorin ska det fungera med vector eller en array av pekare, men det f�r bli till
//en annan g�ng. 7 missiler �r �nd� inte s� mycket och borde inte p�verka n�got.
LEVEL* level;
//***************VARIABLAR F�R SPELOBJEKT**********************



//**********************STYRPARAMETRAR F�R ALIENS SKOTT********************
bool alienFirstShot = false;
int alienMissileCharingTime = 0;
int alienMissileTimer = GetTickCount();
//**********************STYRPARAMETRAR F�R ALIENS SKOTT********************


//****************TIMERS***************************************
int levelCompleteTimer;
int levelCompleteDelay;

int gameOverTimer;
int gameOverDelay;
//****************TIMERS***************************************

void initMenu(HWND window){

	//Skapar upp menyknappar

	newGameButton = createMenuElement(window, NEW_GAME);
	resumeGameButton = createMenuElement(window, RESUME_GAME);
	highscoreButton = createMenuElement(window, HIGHSCORE);
	exitButton = createMenuElement(window, EXIT);
}

void showGameMenu(){
	
	//Visar eller g�mmer spelets meny

	if (!menuVisible){
		stopThemeSong();
		menuVisible = true;
	}
	else{
		playThemeSong();
		menuVisible = false;
	}
}

void showHighscoreList(){

	//Visar eller g�mmer highscorelistan

	if (!highscoreVisible){
		highscoreVisible = true;
	}
	else{
		highscoreVisible = false;
		exitHighscoreList();
	}
}

bool gameInit(HWND window){

	//St�ller iordning spelets startparametrar

	srand((unsigned int)time(NULL)); //Skapar ett nytt slumpfr�
	
	gameOver = true;
	gamePaused = false;
	levelComplete = false;
	splashscreenShowed = true;
	menuVisible = false;
	highscoreVisible = false;
	splashScreenVisible = true;

	//Initierar grafik- och inputenheter
	direct3DInit(window, screenWidth, screenHeight, false);
	directInputInit(window);
	direct3DFontsInit();

	initSound(window);

	//Skapar en muspekare
	mousePointer = createMousePointer(window);
	if (mousePointer == NULL)
		return false;

	initMenu(window); //St�ller iordning menyn

	//Laddar diverse sprites, som t ex n�gra textskyltar och de bl� blocken
	if (!loadSprites(window))
		return false;

	//St�ller in grundv�rdena f�r f�rsta niv�n
	level = createLevel(1, 1, 0, 0, false, false);
	if (level == NULL)
		return false;

	//Skapar upp spelaren
	player = createPlayer(850, 1000, 3, window);
	if (player == NULL)
		return false;

	//Skapar upp spelarens skott
	for (int i = 0; i < PLAYERMISSILES; i++){
		playerMissile[i] = createMissile(PLAYERTYPE);
			if (playerMissile[i] == NULL)
				return false;
	}

	//Skapar upp aliens skott
	for (int i = 0; i < ALIENMISSILES; i++){
		alienMissile[i] = createMissile(ALIENTYPE);
		if (alienMissile[i] == NULL)
			return false;
	}

	int alienDestination; //Punkten som aliens ska r�ra sig mot

	//H�r skapar jag upp tre rader med aliens som alla f�r var sin punkt att r�ra sig mot
	//Hela raden r�r sig mot en punkt, men de tre raderna har olika punkter
	for (int i = 0; i < ALIENS; i++){
		if (i==0 || i==10 || i==20) //Skapar slumptal f�r de tre raderna med aliens.
			alienDestination = rand() % ((screenWidth - 64) - 0) + 0;
		if (i < 10)
			alien[i] = createAlien(TOP, i, alienDestination, window);
		if (i>9 && i < 20)
			alien[i] = createAlien(MIDDLE, i - 10, alienDestination, window); //X-positionen �r slutsiffran i iteratorn, d�rav i-10.
		if (i>19)
			alien[i] = createAlien(BOTTOM, i - 20, alienDestination, window);
		if (alien[i] == NULL)
			return false;
	}
	
	ShowCursor(false); //<-St�nger av muspekaren

	showGameMenu(); //<-Visar spelets meny n�r spelet startar

	return true;
}

void removeMissiles(){
	//G�r igenom alla skott som finns p� spelplanen och inaktiverar dessa
	//vilket f�r spelaren uppfattas som att de plockas bort

	for (int i = 0; i < ALIENMISSILES; i++){
		if (i<PLAYERMISSILES)
			playerMissile[i]->active = false;
		alienMissile[i]->active = false;
	}
}

bool levelCompleted(){

	//En metod som g�r igenom alla aliens f�r att se om de �r f�rst�rda eller ej
	//Om samtliga �r f�rst�rda �r banan klarad.

	if (!levelComplete){
		levelComplete = true;

		for (int i = 0; i <ALIENS; i++){
			if (!alien[i]->destroyed)
				levelComplete = false;
			if (alien[i]->explode)
				levelComplete = false;
		}

		//Om banan �r klar s� rensas missiler och timern startar
		//f�r att det ska vara lite mellanrum mellan niv�erna
		if (levelComplete){
			stopThemeSong();
			playLevelCompleteSound();
			removeMissiles();
			levelCompleteTimer = GetTickCount();
		}
	}
	
	return levelComplete;
}

void resetAliens(){

	//Nollst�ller aliens grundv�rden f�r om de �r kvar i spelet eller ej
	//samt deras explosionsanimation
	
	for (int i = 0; i < ALIENS; i++){
		alien[i]->destroyed = false;
		alien[i]->explode = false;
		alien[i]->explosionFrame = 0;
	}
}

void updateTimers(){
	//G�r igenom timers f�r animationer och andra h�ndelser

	//******************************LADDTID F�R SKOTT*******************************************
	
	if (player->firstShot){
		player->chargingTime = GetTickCount() - player->timeCharged;
	}
	
	//******************************ANIMERING AV SPELARENS SKOTT*******************************************
	for (int i = 0; i <= player->activeMissiles; i++){
		animateMissile(playerMissile[i]);
	}

	//******************************ANIMERING AV EXPLOSIONER*******************************************


	for (int i = 0; i <= ALIENS-1; i++){
		animateAlien(alien[i]);
	}

	animatePlayer(player);
	if (player->destroyed){
		removeMissiles();
		player->deadDelayTime = GetTickCount() - player->startDeadTime;
		//V�ntar 2,5 sekund inann spelaren f�r komma tillbaka s� att ljud etc hinner spela klart
		if (player->deadDelayTime > 2500){
			resetThemeSong();
			playThemeSong();
			player->destroyed = false;
		}
	}

	//******************************MELLANRUM MELLAN NIV�ER*******************************************

	if (levelComplete){
		levelCompleteDelay = GetTickCount() - levelCompleteTimer;
		//Efter fem sekunder s� nolls�lls aliens etc och man g�r upp en niv�
		if (levelCompleteDelay > 5000){
			resetAliens();
			levelUp(level, ALIENMISSILES);
			resetThemeSong(); //Startar om musiken
			playThemeSong();
			levelComplete = false;
		}
	}

	//******************************MELLANRUM MELLAN GAMEOVER OCH MENYN*******************************************
	if (gameOver)
		gameOverDelay = GetTickCount() - gameOverTimer;

}

void pauseGame(){
	
	//Pausar eller opausar spelet

	playPauseSound();

	if (!menuVisible){ 
		if (!gamePaused){
			gamePaused = true;
		}
		else{
			gamePaused = false;
		}		
	}
}



void fireMissile(){
	
	//H�r avfyras spelarens skott. Det finns en timer som g�r att det tar en kvarts sekund mellan varje skott.
	//Vid f�rsta skottet g�rs ingen kontroll mot timern. Sedan s� kollas timern, l�gsta inaktiva skottet
	//letas upp och aktiveras. Ljud f�r skott spelas �ven upp.
	
	if (!player->firstShot){
		playFireMissileSound();
		rechargeMissile(player);
		player->firstShot = true;
		activateMissile(playerMissile[0], player->sprite->x + 23);
	}
	else {
		if (player->chargingTime > 249){ //Timer f�r att ladda skotten. 
			for (int i = 0; i <= PLAYERMISSILES-1; i++){ //Letar upp l�gsta inaktiva missilnumret
 				if (!playerMissile[i]->active){
					playFireMissileSound();
					rechargeMissile(player);
					activateMissile(playerMissile[i], player->sprite->x+23);
					break;
				}
			}
		}
	}
}

bool detectCollision(SPRITE* spriteA, SPRITE* spriteB){
	
	//Funktion f�r cirkel-cirkel-kollisionshantering s� som den anv�nds i kurslitteraturn
	//Den r�knar ut avst�ndet mellan tv� mittpunkter (som i sin tur �r beroende p� hur 
	//stora spritsen �r som skickas in i funktionen). Om avst�ndet �r mindre �n det tv� radierna s� 
	//r�knas det som krock

	double radious1, radious2;

	//Radie f�r objekt 1:

	if (spriteA->width > spriteA->height)
		radious1 = spriteA->width / 2.0;
	else
		radious1 = spriteA->height / 2.0;

	//Mittpunkt f�r objekt 1
	double x1 = spriteA->x + radious1;
	double y1 = spriteA->y + radious1;
	D3DXVECTOR2 vector1(x1, y1);

	//Radie f�r objekt 2:
	if (spriteB->width > spriteB->height)
		radious2 = spriteB->width / 2.0;
	else
		radious2 = spriteB->height / 2.0;

	//Mittpunkt f�r objekt 2:
	double x2 = spriteB->x + radious2;
	double y2 = spriteB->y + radious2;
	D3DXVECTOR2 vector2(x2, y2);

	double deltax = vector1.x - vector2.x;
	double deltay = vector1.y - vector2.y;
	double distance = sqrt((deltax*deltax) + (deltay*deltay));

	return (distance < radious1 + radious2);

}

bool detectBoxCollision(SPRITE* spriteA, SPRITE* spriteB){
	
	//Boxkollision, funktionen tar tv� sprites och r�knar ut en rektangel runt de tv�
	//och om de �verlappar s� returneras en krock

	if (spriteA->x>spriteB->x &&
		(spriteA->x + spriteA->width)<(spriteB->x + spriteB->width) &&
		spriteA->y>spriteB->y &&
		(spriteA->y + spriteA->height) < (spriteB->y + spriteB->height))
		return true;

	return false;

}

void setUpNewGame(){
	
	//Vid varje nytt spel s� rensas spelplanen p� missiler, aliens �terst�lls
	//spelaren �terst�lls och niv�n st�lls om till f�rsta niv�n igen. Menyn st�ngs �ven.
	
	removeMissiles();
	resetAliens();
	
	level->alienFire = false;
	level->missileShield = false;
	level->levelNumber = 1;
	level->alienMissileSpeed = 0;
	level->noOfAliensMissiles = 0;
	level->alienSpeed = 1;

	player->lives = 3;
	player->score = 0;
	player->sprite->x = 850;
	player->sprite->y = 1000;

	gameOver = false;
	showGameMenu();

	resetThemeSong();
	playThemeSong();
}

void checkUserInput(HWND window){
	
	//Hanterar anv�ndarens tangentbordskommanden


	//Kontrollerar s� att spelet inte har tappat input-enheterna, om s� �r fallet s� �tertas de
	if (directInputKeyboard->Poll() != DI_OK)
		directInputKeyboard->Acquire();

	//Samma ska med musen
	if (directInputMouse->Poll() != DI_OK)
		directInputMouse->Acquire();

	//Uppdaterar mus, t-bord etc
	directInputUpdate(window);

	//Om splashsk�rmen �r framme s� kan man bara anv�nda space.
	if (!splashScreenVisible)
	{
		//ESC �ppnar eller st�nger menyn
		if (keyPressed(DIK_ESCAPE)){
			if (!highscoreVisible)
				showGameMenu();
			if (highscoreVisible)
				showHighscoreList();
		}

		//P pauser eller opausar spelet
		if (keyPressed(DIK_P)){
			pauseGame();
		}

		//Pauseknappen g�r samma sak
		if (keyPressed(DIK_PAUSE)){
			pauseGame();
		}

		//Piltangenterna r�r spelaren och space avfyrar skott
		//Men bara om spelet inte �r pausat eller menyn �r ig�ng.
		//Mellan tv� niv�er fungerar inte heller styrtangenterna
		if (!gamePaused){
			if (!menuVisible){
				if (!player->destroyed){
					if (!levelCompleted()){
						if (keyDown(DIK_LEFT))
							movePlayerLeft(player);

						if (keyDown(DIK_RIGHT))
							movePlayerRight(player);

						if (keyPressed(DIK_SPACE))
							fireMissile();
					}
				}
			}
		}

		//Kontrollerar s� att spelaren inte flyttar sin sprite utanf�r sk�rmen
		checkOutOfScreen(player, screenWidth);

		//Musknappar hanteras h�r nedan beroende om menyn �r aktiverad
		//Om menyn �r aktiverad s� g�rs en box-box-kollisionskontroll mot respektive knapps sprite
		//f�rutom om highscorelistan visas

		if (menuVisible){

			setMousePosition(mousePointer, window);

			if (!highscoreVisible){
				if (mouseButton(0)){
					if (detectBoxCollision(mousePointer->sprite, newGameButton->sprite)){
						//New game nollst�ller ett spel
						setUpNewGame();
					}

					if (detectBoxCollision(mousePointer->sprite, resumeGameButton->sprite)){
						//Resume game-knappen g�r bara att klicka p� s� l�nge ett spel �r aktivt
						if (!gameOver)
							showGameMenu();
					}

					if (detectBoxCollision(mousePointer->sprite, highscoreButton->sprite)){
						//Highscoreknappen visar highscorelistan (och skickar ett anrop f�r att initera highscorelistan)
						if (!highscoreVisible)
							setUpHighscoreList();
						showHighscoreList();

					}

					if (detectBoxCollision(mousePointer->sprite, exitButton->sprite)){
						//St�nger av parametern som h�ller huvudloopen i WinMain ig�ng
						gameRunning = false;
					}
				}
			}
		}
	}

	//Tar bort splashsk�rmen vid tryck p� space
	if (splashScreenVisible){
		if (keyPressed(DIK_SPACE))
			splashScreenVisible = false;
	}

}

void moveAliens(){
	
	//Flyttar raderna med aliens p� sk�rmen mot den utslumpade punkten.
	//Den tar den yttersta alien i varje rad och matchar den mot punkten
	//N�r punkten n�s s� slumpas en ny punkt ut
	//Det h�r systemet har ett litet bekymmer och det �r att punkterna kan slumpas ut alldeles f�r n�ra varandra
	//vilket leder till ett ganska ryckigt spelupplevles. Det h�r kan man s�klart l�sa genom att inte l�ta v�rderna vara
	//n�ra varandra men det finns inte tid att l�sa och testa det just nu.
	
	for (int i = 0; i < 3; i++){ 
	//Den h�r yttre loopen anv�nds eftersom det �r tre rader med tio aliens i varje. Genom att multilera radnumret med varje iteration
	//i innerlooparna s� f�r man fram ID:et p� den alien som ska flyttas
		
		//De h�r tv� innerlooparna flyttar aliens �t var sitt h�ll beroende p� var m�lpunkten �r och 
		//niv�ns hastig f�r aliens f�rflyttning
		if (alien[(i * 10)]->sprite->x >= alien[i * 10]->destination){
			for (int j = (i*10); j < (i*10)+10; j++){
				moveAlien(alien[j], 0 - level->alienSpeed);
			}
		}

		if (alien[(i*10)]->sprite->x <= alien[(i*10)]->destination){
			for (int j = (i * 10); j < (i * 10) + 10; j++){
				moveAlien(alien[j], level->alienSpeed);
			}
		}

		//De h�r tv� looparna kontrollerar om den f�rsta eller sista alien i varje rad har n�tt destinationen
		//d� slumpas ett nytt nummer ut. De tar �ven med redan d�da aliens. I en framtida version s� hade det varit snyggare
		//att leta upp den mest v�nstra eller mest h�gra levande alien. S� m�nga ide�r och s� lite tid... :-)
		if (alien[(i*10)]->sprite->x >= alien[(i*10)]->destination - (level->alienSpeed) && alien[(i*10)]->sprite->x <= alien[(i*10)]->destination + level->alienSpeed){
			for (int j = (i * 10); j < (i * 10) + 10; j++){
				alien[j]->destination = rand() % screenWidth;
			}
		}

		if (alien[(i*10)+9]->sprite->x + 34 >= screenWidth - 34){
			for (int j = (i * 10); j <= (i * 10) + 10; j++){
				alien[j]->destination = rand() % screenWidth;
			}
		}
	}
}


void fireAlienMissile(){
	
	//Hanterar aliens skjutande. I princip fungerar det likadant som f�r spelaren med de undantagen
	//att positionen p� skottet utg�r fr�n en slumpm�ssigt utvald, levande, alien och att hastigheten
	//�r variabel beroende p� vilken niv� spelaren �r p�

	//Steg ett �r att hitta en alien som inte �r d�d. L�sningen �r inte s� komplicerad utan den slumpar fram ett
	//v�rde och matchar det mot en alien. �r den d�d s� slumpas ett nytt v�rde fram. Har man extrem otur s� kan det h�r
	//skapa en evighetsloop (och det vill jag inte ha i mitt spel) s� jag har �ven lagt in en kontroll som plockar ut 
	//det l�gsta numret bland de levande aliens om 500 slumptal inte har hittat en levande alien
	
	bool alienExists=false;
	int alienShip;
	
	int tries = 0;
	
	while (!alienExists){
		alienShip = rand() % ALIENS;
		if (alien[alienShip]->destroyed)
			alienExists = false;
		else
			alienExists = true;
		tries++;
		if (tries == 500){
			for (int i = 0; i < ALIENS; i++){
				if (!alien[i]->destroyed){
					alienExists = true;
					alienShip = i;
					break; 
				}
			}
		}
		
	}
	
	//Det h�r fungerar exakt likadant som f�r fireMissile() f�rutom att det �r en alien ist�llet f�r player som skjuter

	if (!alienFirstShot){  //Vid f�rsta skottet anv�nds inte timerkontrollen
		alienFirstShot = true;
		alienMissileCharingTime = 0;
		alienMissileTimer = GetTickCount();
		if (!alien[alienShip]->destroyed){
			playAlienFireSound();
			alienMissile[0]->active = true;
			alienMissile[0]->missileSprite->x = alien[alienShip]->sprite->x + 23; //+23 f�r att f� skotten i mitten av spriten
			alienMissile[0]->missileSprite->y = alien[alienShip]->sprite->y;
		}
	}
	else{
		alienMissileCharingTime = GetTickCount() - alienMissileTimer;
		if (alienMissileCharingTime > level->alienRechargeTime){ //Timer f�r att ladda skotten. Det h�r beh�ver finjusteras
			for (int i = 0; i <= level->noOfAliensMissiles; i++){ //Letar upp l�gsta inaktiva missilnumret
				if (!alienMissile[i]->active){
					alienMissileCharingTime = 0;
					alienMissileTimer = GetTickCount();
					if (!alien[alienShip]->destroyed){
						playAlienFireSound();
						alienMissile[i]->active = true;
						alienMissile[i]->missileSprite->x = alien[alienShip]->sprite->x + 23; //+23 f�r att f� skotten i mitten av spriten
						alienMissile[i]->missileSprite->y = alien[alienShip]->sprite->y;
					}
					break;
				}
			}
		}
	}
}

void alienActions(){
	//G�r igenom aliens r�relser och anfall

	moveAliens();

	//Den h�r loopen g�r igenom om det finns n�gra aliens kvar
	//Utan den s� fastnar spelet n�r den ska slumpa ut en alien som skjuter
	bool alienExists = false;
	for (int i = 0; i < ALIENS; i++){
		if (!alien[i]->destroyed)
			alienExists = true;
	}
	
	if (alienExists){
		if (level->alienFire)
			fireAlienMissile();
	}


}

void moveMissiles(){
	
	//G�r igenom aktiva missiler och flyttar dom upp�t eller ned�t beroende
	//p� vilken missiltyp det �r. Hastigheten styrs av niv�ns missilhastighet

	for (int i = 0; i <= player->activeMissiles; i++){
		moveMissile(playerMissile[i], 0);
	}

	for (int i = 0; i <= level->noOfAliensMissiles; i++){
		moveMissile(alienMissile[i], level->alienMissileSpeed);
	}
}



void checkCollisions(){
	
	//Kontrollerar kollisioner mellan sprites

	//Loopar igenom alla aktiva skott fr�n spelaren och matchar dessa med alla aliens som inte �r f�rst�rda
	for (int i = 0; i <= player->activeMissiles; i++){
		if (playerMissile[i]->active){
			for (int j = 0; j < ALIENS; j++){
				if (!alien[j]->destroyed){
					if (detectCollision(playerMissile[i]->missileSprite, alien[j]->sprite)){
						//Om kollision uppt�cks s� exploderar tr�ffad alien. Po�ng uppdateras, alien plockas, ljud spelas upp etc
						playEnemyExplodeSound();
						alien[j]->destroyed = true;
						alien[j]->explode = true;
						alien[j]->startExplosionTime = GetTickCount();
						playerMissile[i]->active = false;
						if (alien[j]->position == TOP)
							player->score += 200 * (level->alienSpeed);
						if (alien[j]->position == MIDDLE)
							player->score += 150 * (level->alienSpeed);
						if (alien[j]->position == BOTTOM)
							player->score += 100 * (level->alienSpeed);
					}
				}
			}
		}
	}

	//Krock aliens skott med spelaren
	//N�r spelaren blir tr�ffad s� p�b�rjas explosionsanimeringen samt att ljud spelas upp
	for (int i = 0; i <= level->noOfAliensMissiles; i++){
		if (alienMissile[i]->active){
			if (detectCollision(alienMissile[i]->missileSprite, player->sprite)){
				alienMissile[i]->active = false;
				player->explode = true;
				player->destroyed = true;
				player->lives--;
				playEnemyExplodeSound();
				stopThemeSong();
				//Om det var spelarens sista liv s� spelas gameovermusiksnutten ist�llet f�r playerdies-musiken
				if (player->lives < 1)
					playGameOverSound();
				else
					playPlayerDiesSound();
				player->startDeadTime = GetTickCount();
			}
			
 			//Krock aliens skott med spelarens skott (spelarens skott f�rsvinner)
			if (level->missileShield){
				for (int j = 0; j <= player->activeMissiles; j++){
					if (detectCollision(alienMissile[i]->missileSprite, playerMissile[j]->missileSprite)){
						playBumpSound();
						playerMissile[j]->active = false;
					}
				}
			}
		}
	}
}

void renderGraphics(HWND window){
	
	//Ritar upp grafiken p� sk�rmen

	direct3DDevice->ColorFill(surfaceBackbuffer, NULL, D3DCOLOR_XRGB(7, 155, 176)); //Fyller backbuffern med ljusbl� f�rg
	direct3DDevice->BeginScene();
		
	if (!splashScreenVisible)
	{
		//Om menyn inte �r aktiv s� ska spelet ritas ut.

		if (!menuVisible){

			drawClouds(); //<-Ritar ut moln i bakgrunden

			//F�rst s� ritas en svart rektangel ut l�ngst upp (egentligen �r det en svart bild som smetas ut �ver den delen av sk�rmen)
			RECT topArea = { 0, 0, 1600, 128 };
			direct3DDevice->StretchRect(topSurface, NULL, surfaceBackbuffer, &topArea, D3DTEXF_NONE);

			//Sen lite information om po�ng och vilken niv� i textform
			RECT scoreArea = { 96, 32, 480, 128 };
			string scoreText = "Score: " + to_string(player->score);
			direct3DScoreFont->DrawTextA(NULL, scoreText.c_str(), -1, &scoreArea, DT_CENTER, D3DCOLOR_XRGB(238, 20, 20));

			RECT levelArea = { 576, 32, 960, 128 };
			string levelText = "Level: " + to_string(level->levelNumber);
			direct3DScoreFont->DrawTextA(NULL, levelText.c_str(), -1, &levelArea, DT_CENTER, D3DCOLOR_XRGB(238, 20, 20));

			//Antal liv. Ett hj�rta per liv ritas ut
			for (int i = 0; i < player->lives; i++){
				lifeSprite->Begin(D3DXSPRITE_ALPHABLEND);
				D3DXVECTOR3 lifeSpritePos(1400 - (64 * i) - 64, 16, 0);
				lifeSprite->Draw(lifeTexture, NULL, NULL, &lifeSpritePos, D3DCOLOR_XRGB(255, 255, 255));
				lifeSprite->End();
			}

			//Ramen med bl�a block ritas ut
			drawBlueBlocks(screenWidth);
			
			//Om det inte �r mellan tv� niv�er ritas spelarens sprite ut
			if (!levelCompleted()){
				drawPlayerSprite(player);
			}

			//Spelarens skott ritas ut
			for (int i = 0; i <= player->activeMissiles; i++){
				drawMissile(playerMissile[i]);
			}

			//Aliens skott ritas ut
			for (int i = 0; i <= level->noOfAliensMissiles; i++){
				drawMissile(alienMissile[i]);
			}

			//Aliens ritas ut
			for (int i = 0; i <= ALIENS - 1; i++){
				drawAlien(alien[i]);
			}

			//En sprite med text som f�rklarar att niv�n �r avklarad
			if (levelCompleted()){
				levelCompleteSprite->Begin(D3DXSPRITE_ALPHABLEND);
				D3DXVECTOR3 levelCompleteSignPos(screenWidth / 2 - 194, screenHeight / 2 - 60, 0);
				levelCompleteSprite->Draw(levelCompleteTexture, NULL, NULL, &levelCompleteSignPos, D3DCOLOR_XRGB(255, 255, 255));
				levelCompleteSprite->End();
			}

			//En gameover-skylt visas om spelet �r slut
			if (gameOver){
				if (!menuVisible){
					gameOverSprite->Begin(D3DXSPRITE_ALPHABLEND);
					D3DXVECTOR3 gameOverSignPos(screenWidth / 2 - 194, screenHeight / 2 - 60, 0);
					gameOverSprite->Draw(gameOverTexture, NULL, NULL, &gameOverSignPos, D3DCOLOR_XRGB(255, 255, 255));
					gameOverSprite->End();
				}
			}

			//En pausskylt skrivs om spelaren pausar spelet
			if (gamePaused){
				pauseSprite->Begin(D3DXSPRITE_ALPHABLEND);
				D3DXVECTOR3 pauseSignPos(screenWidth / 2-290, screenHeight / 2 - 60, 0);
				pauseSprite->Draw(pauseTexture, NULL, NULL, &pauseSignPos, D3DCOLOR_XRGB(255, 255, 255));
				pauseSprite->End();
			}
		}

		if (menuVisible){

			//Menyns knappar ritas upp. Om highscore �r valt s� ritas highscorelistan ut
			if (!highscoreVisible){
				drawMenuElement(newGameButton);
				drawMenuElement(resumeGameButton);
				drawMenuElement(highscoreButton);
				drawMenuElement(exitButton);
			}

			//Muspekaren ritas ut, den anv�nds bara i menyl�get
			drawMousePointer(mousePointer, window);

			if (highscoreVisible){
				drawHighscoreScreen();
			}
		}
	}

	//Ritar ut splashsk�rmen
	if (splashScreenVisible){
		RECT splashScreenArea = { 0, 0, screenWidth, screenHeight };
		direct3DDevice->StretchRect(splashScreen, NULL, surfaceBackbuffer, &splashScreenArea, D3DTEXF_NONE);
	}

	//Avslutar uppritningen och nollar DirectX-enheten
	direct3DDevice->EndScene(); 
	direct3DDevice->Present(NULL, NULL, NULL, NULL);
}

void checkGameOver(){
	
	//Kollar om spelarens liv har tagit slut. 
	//Om det spelaren blir gameover s� startar en timer och sedan visas menyn igen
	//Highscores ber�knas �ven. Planen var �ven att man skulle f� mata in namn etc
	//men tiden r�cker inte till f�r att g�ra det gr�nsnittet just nu. Ytterliggare
	//en punkt f�r ett senare projekt.
	
	if (player->lives < 1){
		if (!gameOver){
			newHighScore(player->score, level->levelNumber);
			gameOverTimer = GetTickCount();
		}
		gameOver = true;
		if (gameOverDelay>2500){
			showGameMenu();
		}
	}
}

void gameLoop(HWND window){
	
	//Det h�r �r spelloppen. Den g�r f�ljande:
	// Den kontrollerar alltid spelarens inmatning
	// Om menyn inte visas eller om spelet inte �r pausat s� uppdaterar den eventuella timers
	// kontrollerar gameover-vilkor samt scrollar bakgrunden. Om spelaren inte �r gameover eller om niv�n inte �r avklarad
	// s� f�r aliens agera, skott flyttas och kollisioner kontrolleras.
	// Sista s� ritas grafiken ut, det h�nder i varje iteration oavsett ovanst�ende vilkor.

	checkUserInput(window);
	if (!gamePaused){
		if (!menuVisible){
			updateTimers();
			moveClouds();
			checkGameOver();
			if (!gameOver){
				if (!levelCompleted()){
					if (!player->destroyed){
						alienActions();
						moveMissiles();
						checkCollisions();
					}
				}
			}			
		}
	}
	renderGraphics(window);
}

void gameEnd(){
	//St�dar upp objekt och pekare som beh�ver tas bort eller avslutas

	//Raderar aliens missiler (se l�ngre ner om varf�r den �r 1-6 och inte 0-6).
	
	for (int i = 1; i<ALIENMISSILES; i++){
		destroyMissile(alienMissile[i]);
	}
	
	//Tar bort spelarens skott
	for (int i = 0; i < PLAYERMISSILES; i++){
		destroyMissile(playerMissile[i]);		
	}

	//Tar bort spelaren
	destroyPlayer(player);
	
	//Tar bort aliens
	for (int i = 0; i < ALIENS; i++){
		destroyAlien(alien[i]); 
	}



	//Tar bort bakgrundsmoln
	destroyClouds();

	//Tar bort muspekaren
	destroyMousePointer(mousePointer);
	
	//Tar bort menyknapparna
	destroyMenuElement(newGameButton);
	destroyMenuElement(resumeGameButton);
	destroyMenuElement(highscoreButton);
	destroyMenuElement(exitButton);

	//St�nger ner ljudenheten
	destroySoundDevice();

	//Ger tillbaka input till Windows
	directInputShutdown();
	
	//St�nger ner DirectX-enheten
	direct3DShutdown();


	//destroyMissile(alienMissile[0]);
	//Det h�r kr�ver s�kert sin f�rklaring; jag f�r en minneskonflikt n�gonstans n�r jag f�rs�ker plocka bort element 0 i arrayen med fiendeskott
	//Jag har faktiskt ingen aning om varf�r. Jag ser ingen skillnad h�r mot t ex spelarens skott eller arryen med aliens. Jag la kodsnutten h�r 
	//eftersom jag inte lyckas l�sa problemet och d� kan jag iaf st�da upp s� mycket som m�jligt innan det sm�ller. F�r jag n�got tips om varf�r 
	//det blir fel s� vore det uppskattat d� jag lagt flera dagar p� det h�r problemet utan framg�ng. N�r jag s�tter ut breakpoints s� ser jag inget konstigt heller. 0 ser likadan ut som de �vriga (1-6).

}