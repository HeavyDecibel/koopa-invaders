#ifndef __SOUND_H_INCLUDED__
#define __SOUND_H_INCLUDED__

#include "DirectSound.h"
#include "Direct3DGraphics.h"

void initSound(HWND);
void playPauseSound();
void playFireMissileSound();
void playEnemyExplodeSound();
void playThemeSong();
void stopThemeSong();
void resetThemeSong();
void playLevelCompleteSound();
void playPlayerDiesSound();
void playAlienFireSound();
void playGameOverSound();
void playBumpSound();

void destroySoundDevice();

#endif