
#ifndef __HIGHSCORELIST_H_INCLUDED__
#define __HIGHSCORELIST_H_INCLUDED__

#include "Menu.h"
#include <fstream>
#include <string>


//Enkel struct f�r po�ng och niv�
struct HIGHSCOREENTRY{

	int score;
	int level;

};

void readHighscoreFromFile(HIGHSCOREENTRY* highscoreEntry, int rowNo);
void setUpHighscoreList();
void drawHighscoreScreen();
void exitHighscoreList();
bool newHighScore(int score, int level);

#endif