#include "Sound.h"

CSoundManager* directSound = new CSoundManager();

//Ljudbuffertar
CSound* pauseSound;
CSound* fireMissileSound;
CSound* enemyExplodeSound;
CSound* mainThemeSound;
CSound* levelCompleteSound;
CSound* playerDiesSound;
CSound* alienFireSound;
CSound* gameOverSound;
CSound* bumpSound;

void loadSounds(){

	//Laddar in ljud i buffertarna fr�n wav-filer
	directSound->Create(&pauseSound, "sounds/pause.wav");
	directSound->Create(&fireMissileSound, "sounds/fireball.wav");
	directSound->Create(&enemyExplodeSound, "sounds/alienexplode.wav");
	directSound->Create(&mainThemeSound, "sounds/maintheme.wav");
	directSound->Create(&levelCompleteSound, "sounds/levelComplete.wav");
	directSound->Create(&playerDiesSound, "sounds/playerdies.wav");
	directSound->Create(&alienFireSound, "sounds/alienfire.wav");
	directSound->Create(&gameOverSound, "sounds/gameover.wav");
	directSound->Create(&bumpSound, "sounds/bump.wav");

}




void initSound(HWND window){

	//Skapar upp ett objekt av CSoundManager med h�gsta prioritet;
	directSound->Initialize(window, DSSCL_PRIORITY);
	directSound->SetPrimaryBufferFormat(2, 22050, 16);

	loadSounds();

}

//H�r ned �r ett g�ng funktioner som bara spelar upp en ljudfil
//De anropas sedan fr�n andra st�llen i GameEngine.cpp

void playBumpSound(){

	bumpSound->Reset();
	bumpSound->Play();

}

void playGameOverSound(){

	gameOverSound->Play();

}

void playAlienFireSound(){

	alienFireSound->Reset();
	alienFireSound->Play();

}

void playLevelCompleteSound(){
	levelCompleteSound->Reset();
	levelCompleteSound->Play();
}

void playPlayerDiesSound(){
	playerDiesSound->Play();
}

void playThemeSong(){

	mainThemeSound->Play(0, DSBPLAY_LOOPING); //<-Spelar upp musiken i en slinga
}

void stopThemeSong(){
	
	mainThemeSound->Stop(); //<-Pausar musiken
}

void resetThemeSong(){

	mainThemeSound->Reset(); //<-Nollst�ller musiken

}

void playEnemyExplodeSound(){
	
	enemyExplodeSound->Reset(); //<-Resetar ljudet h�r s� att bara den sista tr�ffen l�ter
	enemyExplodeSound->Play();

}

void playPauseSound(){

	pauseSound->Play();
		
}

void playFireMissileSound(){
	
	fireMissileSound->Play();

}

void destroySoundDevice(){

	//Avallokerar minnet f�r ljudenheten

	delete directSound;

}