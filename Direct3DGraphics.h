#ifndef __GRAPHICS_H_INCLUDED__
#define __GRAPHICS_H_INCLUDED__

#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <ctime>
#include <iostream>
#include <iomanip>

extern int screenWidth;
extern int screenHeight;

using namespace std;

//Bibliotek f�r DirectX (version 9 d� det �r samma som t�cks i kurslitteraturen)
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "dsound.lib") //Anv�nds f�r CWaveFile i DirectSound.h
#pragma comment(lib, "winmm.lib") //Anv�nds f�r CWaveFile i DirectSound.h
#pragma comment(lib, "dxerr.lib") //Anv�nds f�r CWaveFile i DirectSound.h


//Direct3D-objekt
extern LPDIRECT3D9 direct3DInterface; //Gr�nsnittet (chefen �ver de �vriga objekten)
extern LPDIRECT3DDEVICE9 direct3DDevice; //DX-enheten
extern LPDIRECT3DSURFACE9 surfaceBackbuffer; //Backbufferytan


//Direct3D-funktioner
//Se Direct3DGraphics.cpp f�r kommentarer
bool direct3DInit(HWND hwnd, int width, int height, bool fullscreen);
void direct3DShutdown();
LPDIRECT3DSURFACE9 loadSurface(string filename);
void drawSurface(LPDIRECT3DSURFACE9 dest, float x, float y, LPDIRECT3DSURFACE9 source);

LPDIRECT3DTEXTURE9 loadTexture(string filename, D3DCOLOR transcolor = D3DCOLOR_XRGB(0, 0, 0));

#endif
