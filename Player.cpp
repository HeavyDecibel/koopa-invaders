#include "Player.h"

bool loadPlayerSprite(PLAYER* player, HWND window){

	//Laddar texture f�r spelarens sprite och skapar sedan spriten

	player->sprite->texture= loadTexture("graphics/cloudMario.tga", (255,0,255));
	if (!player->sprite->texture)
	{
		MessageBox(window, "Could not load spritefiles", "Error", MB_ICONERROR);
		return false;
	}
	D3DXCreateSprite(direct3DDevice, &player->sprite->sprite);

	return true;
}

PLAYER* createPlayer(float x, float y, int lives, HWND window){
	
	//Skapar upp spelaren och s�tter dess initialattribut som po�ng, liv etc
	//H�r skapas �ven spriten f�r spelaren upp

	PLAYER* player = new PLAYER();

	player->score = 0;
	player->shotsFired = 0;
	player->firstShot = false;	
	player->activeMissiles = 3; //<-anv�nds f�r att h�lla kolla p� hur  m�nga missiler som spelaren kan skjuta
	
	player->lives = lives;
	
	player->explode = false;
	player->explosionFrame = 0;
	player->destroyed = false;

	player->sprite = new SPRITE();
	player->sprite = createSprite(32, 64, 0, 0, 0, x, y, 0);
	if (!loadPlayerSprite(player, window))
	{
		destroyPlayer(player);
		return NULL;
	}
	return player;
}

void destroyPlayer(PLAYER* player){
	
	//Avallokerar minne f�r spelaren och dess sprite
	
	destroySprite(player->sprite);
	delete player;
}

void movePlayerLeft(PLAYER* player){
	
	//Flyttar spelaren �t v�nster
	
	player->sprite->x -= 6.0f;
}

void movePlayerRight(PLAYER* player){
	
	//Flyttar spelaren �t h�ger
	
	player->sprite->x += 6.0f;
}

void drawPlayerSprite(PLAYER* player){
	
	//Ritar ut spelarens sprite p� sk�rmen
	//H�r finns tv� olika spritar, en f�r spelaren (som inte �r animeringsbar) och en som visa n�r spelaren exploderar
	//Om spelaren �r d�d s� ritas ingen sprite ut

	player->sprite->sprite->Begin(D3DXSPRITE_ALPHABLEND);
		D3DXVECTOR3 playerPosition(player->sprite->x, player->sprite->y, 0);
		
		if (player->explode){
			RECT spriteMap = { 120 + (player->explosionFrame * 33), 1542, 120 + 33 + (player->explosionFrame * 33), 1542 + 33 }; //l�gg det h�r i spritestructen sedan
			player->sprite->sprite->Draw(spriteSheet, &spriteMap, NULL, &playerPosition, D3DCOLOR_XRGB(255, 255, 255));
		}
		else{
			if (!player->destroyed)
				player->sprite->sprite->Draw(player->sprite->texture, NULL, NULL, &playerPosition, D3DCOLOR_XRGB(255, 255, 255));
		}
			
	player->sprite->sprite->End();
}

void checkOutOfScreen(PLAYER* player, int screenWidth){
	
	//Kollar om spelaren n�r sk�rmens ytterkanter. Om s� h�nder s� flyttas den tillbaka s� att den inte g�r utanf�r sk�rmen
	
	if (player->sprite->x < 0)
		player->sprite->x = 0;

	if (player->sprite->x > screenWidth - 64)
		player->sprite->x = screenWidth - 64;
}

void rechargeMissile(PLAYER* player){

	//Varje g�ng spelaren skjuter s� "laddas" skotten om, dvs en timer nollst�lls och startas

	player->shotsFired++;
	player->chargingTime = 0;
	player->timeCharged = GetTickCount();
}

void animatePlayer(PLAYER* player){
	
	//Animerar spelarens explosionssprite. Fungerar som f�r aliens,
	//dvs en timer har koll p� n�r den ska byta animationsruta
	//N�r animeringen �r klar s� nollst�lls explosionen s� att 
	//den g�r att anv�nda igen.
	
	if (player->explode){
		player->explosionDelayTime = GetTickCount() - player->startExplosionTime;
		if (player->explosionDelayTime > 100){
			player->startExplosionTime = GetTickCount();
			player->explosionFrame++;
			if (player->explosionFrame == 3){
				player->explode = false;
				player->explosionFrame = 0;
			}
		}
	}
}