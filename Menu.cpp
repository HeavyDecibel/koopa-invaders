#include "Menu.h"

bool loadMenuSprite(MENUELEMENT* menuElement, HWND window){

	//Laddar spriten f�r menyknapparna. Om n�got g�r fel s� returneras falskt

	menuElement->sprite->texture = loadTexture("graphics/menuButton.png", (255, 0, 255));
	if (!menuElement->sprite->texture)
	{
		MessageBox(window, "Could not load spritefiles", "Error", MB_ICONERROR);
		return false;
	}
	//Spriten skapas
	D3DXCreateSprite(direct3DDevice, &menuElement->sprite->sprite);

	return true;
}


MENUELEMENT* createMenuElement(HWND window, menuType type){
	
	//Skapar upp en menyknapp med sprite och position beroende vad det �r f�r typ av knapp

	MENUELEMENT* menuElement = new MENUELEMENT();

	float x, y;

	menuElement->type = type;

	//S�tter v�rden f�r position beroende p� vad det �r f�r knapp
	if (menuElement->type == NEW_GAME){
		x = (screenWidth / 2) - 200;
		y = 200;
	}

	if (menuElement->type == RESUME_GAME){
		x = (screenWidth / 2) - 200;
		y = 300;
	}

	if (menuElement->type == HIGHSCORE){
		x = (screenWidth / 2) - 200;
		y = 400;
	}

	if (menuElement->type == EXIT){
		x = (screenWidth / 2) - 200;
		y = 600;
	}

	//Anledningen till att knapparna har en sprite �r att de ska g� att g�ra kollisionstester mot
	menuElement->sprite = new SPRITE();
	menuElement->sprite = createSprite(400, 120, 0, 0, 0, x, y, 0);

	if (!loadMenuSprite(menuElement, window))
	{
		destroyMenuElement(menuElement);
		return NULL;
	}

	return menuElement;

}

bool loadMouseSprite(MOUSEPOINTER* mousePointer, HWND window){

	//�ven muspekaren f�r en sprite, i samma syfte som menyknapparna; det ska g� att g�ra krocktester med den

	mousePointer->sprite->texture = loadTexture("graphics/MouseCursor.tga", (255, 0, 255));
	if (!mousePointer->sprite->texture)
	{
		MessageBox(window, "Could not load spritefiles", "Error", MB_ICONERROR);
		return false;
	}
	D3DXCreateSprite(direct3DDevice, &mousePointer->sprite->sprite);

	return true;
}


MOUSEPOINTER* createMousePointer(HWND window){

	//En muspekare skapas, eller r�ttare sagt en grafisk avbild av en muspekare.

	MOUSEPOINTER* mousePointer = new MOUSEPOINTER();

	mousePointer->sprite = new SPRITE();
	mousePointer->sprite = createSprite(20, 20, 0, 0, 0, 0, 0, 0);

	if (!loadMouseSprite(mousePointer, window))
	{
		destroyMousePointer(mousePointer);
		return NULL;
	}

	return mousePointer;

};

void setMousePosition(MOUSEPOINTER* mousePointer, HWND window){
	
	//Tar reda p� muspekarens x- och y-position
	//H�r fick jag inte till det riktigt bra, f�r det blir en offset mot 
	//windows muspekare vilket bland annat f�r som effekt att man inte kan flytta runt den 
	//i hela sk�rmen. I just det h�r fallet g�r det ingenting d� ingenting klickbart finns
	//i sk�rmens nederdel, men lite irriterande �r det.

	mousePointer->sprite->x = mousePosition(window).x;
	mousePointer->sprite->y = mousePosition(window).y;
}

void drawMousePointer(MOUSEPOINTER* mousePointer, HWND window){

	//Ritar ut muspekarens sprite p� sk�rmen utifr�n spritens kordinater

	mousePointer->sprite->sprite->Begin(D3DXSPRITE_ALPHABLEND);
		D3DXVECTOR3 mouseSpritePos(mousePointer->sprite->x, mousePointer->sprite->y, 0);
		mousePointer->sprite->sprite->Draw(mousePointer->sprite->texture, NULL, NULL, &mouseSpritePos, D3DCOLOR_XRGB(255, 255, 255));
	mousePointer->sprite->sprite->End();

}

void drawMenuElement(MENUELEMENT* menuElement){

	//Ritar ut menyelement.
	//Det ritas ut en knapp som h�r till varje menyelement. Knappen �r en sprite
	//Texten i varje knapp �r dock en vanlig font.

	string menuText;

	menuElement->sprite->sprite->Begin(D3DXSPRITE_ALPHABLEND);
		D3DXVECTOR3 menuPosition(menuElement->sprite->x, menuElement->sprite->y, 0);
		menuElement->sprite->sprite->Draw(menuElement->sprite->texture, NULL, NULL, &menuPosition, D3DCOLOR_XRGB(255, 255, 255));	
	menuElement->sprite->sprite->End();

	RECT menuElementRectangle = {menuElement->sprite->x, menuElement->sprite->y+20, menuElement->sprite->x+menuElement->sprite->width, menuElement->sprite->y+menuElement->sprite->height+20};
	
	if (menuElement->type == NEW_GAME)
		menuText = "New game";
	if (menuElement->type == RESUME_GAME)
		menuText = "Resume game";
	if (menuElement->type == HIGHSCORE)
		menuText = "Highscores";
	if (menuElement->type == EXIT)
		menuText = "Exit";
	
	direct3DMenuFont->DrawTextA(NULL, menuText.c_str(), -1, &menuElementRectangle, DT_CENTER, D3DCOLOR_XRGB(238, 20, 20));

}

void destroyMenuElement(MENUELEMENT* menuElement){

	//Avallokerar minne f�r menyelementet och dess sprite
	
	delete menuElement->sprite;
	delete menuElement;

}

void destroyMousePointer(MOUSEPOINTER* mousePointer){
	
	//Avallokerar minne f�r muspekaren och dess sprite
	
	delete mousePointer->sprite;
	delete mousePointer;
}