
#include "DirectInput.h"

//DirectInput-objekt
LPDIRECTINPUT8 directInputInterface; //Gr�nsnittet
LPDIRECTINPUTDEVICE8 directInputMouse; //Musenhet (kommer inte anv�ndas i spelet, bara f�r menyn)
LPDIRECTINPUTDEVICE8 directInputKeyboard; //Tangentbordsenhet
DIMOUSESTATE mouseState; //F�r att returnera musens position, knapptryck etc
char keys[256]; //Upps�ttning tangenter
int keyPressState[256];

int mousePosX = 0;
int mousePosY = 0;


bool directInputInit(HWND window){
	
	
	//Initierar DirectInputgr�nsnittet	
	HRESULT result = DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&directInputInterface, NULL);

	//Som sedan anv�nds f�r att s�tta upp mus- och tangentbordsenhet
	//med r�ttigheter och korrekt format
	directInputInterface->CreateDevice(GUID_SysKeyboard, &directInputKeyboard, NULL);
	directInputKeyboard->SetDataFormat(&c_dfDIKeyboard);
	directInputKeyboard->SetCooperativeLevel(window, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	directInputKeyboard->Acquire();

	directInputInterface->CreateDevice(GUID_SysMouse, &directInputMouse, NULL);
	directInputMouse->SetDataFormat(&c_dfDIMouse);
	directInputMouse->SetCooperativeLevel(window, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	directInputMouse->Acquire();

	return true;
}

void directInputUpdate(HWND window){

	//Kollar statusen p� inputenheterna
	directInputMouse->GetDeviceState(sizeof(mouseState), (LPVOID)&mouseState);
	directInputKeyboard->GetDeviceState(sizeof(keys), (LPVOID)&keys);

	//Kollar muspositionen
	mousePosX += mouseState.lX;
	mousePosY += mouseState.lY;

}

void directInputShutdown(){
	//St�nger ner DirectInput-objekten och ger tillbaka dom till Windows.

	if (directInputKeyboard){
		directInputKeyboard->Unacquire();
		directInputKeyboard->Release();

	}

	if (directInputMouse){
		directInputMouse->Unacquire();
		directInputMouse->Release();

	}
}



int keyDown(int key){
	//Returnerar nedtryckt tangent
	return (keys[key] & 0x80);
}


bool keyUp(DWORD key)
{
	//Returnerar sl�ppt tangent
	if (keys[key] & 0x80){
		return false;
	}
	else {
		return true;
	}
}

int mouseButton(int button){
	//Returnerar musknapp
	return mouseState.rgbButtons[button] & 0x80;
}

POINT mousePosition(HWND window){
	
	//Returnerar musen postion som en POINT, men den g�r �ven punkten relativt till spelf�nstret
	//Jag har dock inte f�tt det h�r att fungera helt perfekt, men det f�r duga f�r det h�r spelet

	POINT currentMousePosition;
	GetCursorPos(&currentMousePosition);
	ScreenToClient(window, &currentMousePosition);

	return currentMousePosition;
}

int mouseX(){
	//Returnerar musens X-position
	return mousePosX;
}

int mouseY(){
	//Returnerar musens Y-position
	return mousePosY;
}

bool keyPressed(DWORD key)
{
	
	//Den h�r funktionen �r inte min fr�n b�rjan. Det �r en funktion jag hittade p� n�tet
	//n�r jag letade efter ett s�tt att f� en mindre k�nslig keyDown-funktion. Den tidigare har n�mligen problemet
	//med att om man t ex trycker p� ESC s� kan det r�knas som flera nedslag och d� blinkar menyn.

	//Hur som helst s� motsvarar den keyPressed i .NET, dvs den g�r n�got s� l�nge man h�ller nere en tangent
	//och sedan sl�pper upp den igen
	
	//Om tangent �r nedtryckt
	if (keyDown(key)){
		keyPressState[key] = 1; //s� s�tts keyPressState till 1
	}
	
	//Om keyPressState �r ett och en tangent sl�pps upp...
	if (keyPressState[key] == 1){
		if (keyUp(key))
			keyPressState[key] = 2;//...s� s�tts keyPressSate till 2
	}

	//Om keyPressStat �r 2, dvs en tangent har blivit nedtryckt och sedan uppsl�ppt
	//s� nollas keyPressState och sant returneras, vilket inneb�r att en tangent har blivit
	//nedtryckt och uppsl�ppt. Hur l�ng tid spelar ingen roll.
	if (keyPressState[key] == 2){
		keyPressState[key] = 0;
		return true;
	}

	return false;
}


