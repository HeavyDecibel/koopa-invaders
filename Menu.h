#ifndef __MENU_H_INCLUDED__
#define __MENU_H_INCLUDED__

#include "Sprites.h"
#include "DirectInput.h"

//Enum f�r vilka menytyper som finns
enum menuType{NEW_GAME, RESUME_GAME, HIGHSCORE, EXIT};

//Muspekaren har en struct f�r att inneh�lla spriten
struct MOUSEPOINTER{
	SPRITE* sprite;
};

struct MENUELEMENT{
	SPRITE* sprite;
	menuType type;
};

MENUELEMENT* createMenuElement(HWND window, menuType type);
MOUSEPOINTER* createMousePointer(HWND window);

void destroyMenuElement(MENUELEMENT* menuElement);
void destroyMousePointer(MOUSEPOINTER* mousePointer);

void drawMousePointer(MOUSEPOINTER* mousePointer, HWND window);
void drawMenuElement(MENUELEMENT* menuElement);

void setMousePosition(MOUSEPOINTER* mousePointer, HWND window);

#endif