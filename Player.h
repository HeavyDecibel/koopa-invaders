
#ifndef __PLAYER_H_INCLUDED__
#define __PLAYER_H_INCLUDED__

#include "Direct3DGraphics.h"
#include "Sprites.h"

struct PLAYER{
	
	int lives; //antal liv spelaren har kvar
	int score; //och antal po�ng
	int shotsFired; //antal skott spelaren har skjutit. F�r statistik, men �ven f�r att kicka ig�ng timern
	bool firstShot; //om f�rsta skottet �r avlossat eller ej
	bool explode; //om spelaren har exploderat
	bool destroyed; //om spelaren �r f�rst�rd

	//Timrar f�r explosioner och mellan det att spelaren d�r och f�r komma tillbaka igen
	//men �ven f�r hur l�ng tid det tar att ladda skott
	int explosionDelayTime; 
	int startDeadTime;
	int deadDelayTime;
	int startExplosionTime;
	int explosionFrame;
	int chargingTime;
	int timeCharged;

	int activeMissiles; //Hur m�nga skott som �r "aktiva" p� sk�rmen, dvs s�dana som inte har g�tt utanf�r sk�rmen eller tr�ffat en fiende

	//Spelarens sprite
	SPRITE* sprite;

};

PLAYER* createPlayer(float x, float y, int lives, HWND window);
void movePlayerLeft(PLAYER* player);
void movePlayerRight(PLAYER* player);
void destroyPlayer(PLAYER* player);
void drawPlayerSprite(PLAYER* player);
void checkOutOfScreen(PLAYER* player, int screenWidth);
void rechargeMissile(PLAYER* player);
void animatePlayer(PLAYER* player);

#endif