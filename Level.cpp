
#include "Level.h"

LEVEL* createLevel(int levelNumber, int alienSpeed, int alienMissileSpeed, int noOfAliensMissiles, bool missileShield, bool alienFire){
	
	//Skapar upp en level. Samma level anv�nds sedan igenom hela spelet men dess v�rden �ndras
	//V�derna som finns styr bl a hur fort aliens och dess missiler r�r sig
	//om aliens f�r skjuta tillbaka, hur l�ng tid det tar att ladda om dess skott 
	//om aliens skott kan blockera spelarens skott etc
	
	LEVEL* level = new LEVEL();
	level->levelNumber = levelNumber;
	level->alienSpeed = alienSpeed;
	level->alienMissileSpeed = alienMissileSpeed;
	level->noOfAliensMissiles = noOfAliensMissiles;
	level->missileShield = missileShield;
	level->alienFire = alienFire;
	level->alienRechargeTime = 500;
	return level;
}

void levelUp(LEVEL* level, int maxMissiles){

	//�ndrar v�rdena f�r niv�n beroende p� vilken niv� som �r n�sta

	level->levelNumber++;
	level->alienSpeed = level->levelNumber;
	level->alienMissileSpeed = level->levelNumber * 2;

	switch (level->levelNumber){
		case 2:
			level->alienFire = true;
			level->missileShield = false;
			level->noOfAliensMissiles = 2;
			break;

		case 3:
			level->alienFire = true;
			level->missileShield = false;
			level->noOfAliensMissiles = 2;
			break;

		case 4:
			level->alienFire = true;
			level->missileShield = false;
			level->noOfAliensMissiles = 3;
			level->alienRechargeTime = 250;
			break;

		case 5:
			level->alienFire = true;
			level->missileShield = false;
			level->noOfAliensMissiles = 4;
			level->alienRechargeTime = 150;
			break;

		case 6:
			level->alienFire = true;
			level->missileShield = true;
			level->noOfAliensMissiles = 5;
			break;

		case 7:
			level->alienFire = true;
			level->missileShield = true;
			level->noOfAliensMissiles = 5;
			level->alienRechargeTime = 100;
			break;

		case 8:
			level->alienFire = true;
			level->missileShield = true;
			level->noOfAliensMissiles = 5;
			level->alienRechargeTime = 50;
			break;

	}

	if (level->levelNumber > 8){
		level->alienFire = true;
		level->missileShield = true;
		level->alienRechargeTime = 25;
		if (level->noOfAliensMissiles<maxMissiles) //<-Ser till att det inte kan bli f�r m�nga alienskott 
			level->noOfAliensMissiles++;
	}
}

void destroyLevel(LEVEL* level){
	//Avallokerar niv�ns minne
	delete level;
}