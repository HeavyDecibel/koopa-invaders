
//Allting i den h�r filen kommer p� ett eller annat s�tt fr�n kurslitteraturen, en del funktioner har jag �ndrat n�got p� dock

#include "Direct3DGraphics.h"

LPDIRECT3D9 direct3DInterface; //Gr�nsnittet (chefen �ver de �vriga objekten)
LPDIRECT3DDEVICE9 direct3DDevice; //DX-enheten
LPDIRECT3DSURFACE9 surfaceBackbuffer; //Backbufferytan

bool direct3DInit(HWND window, int width, int height, bool fullscreen){
	
	//Funktion f�r att initiera DirectX
	
	//Gr�nsnittet skapas upp
	direct3DInterface = Direct3DCreate9(D3D_SDK_VERSION);
	if (!direct3DInterface)
		return false;

	//St�ller in DirectX-parametrar
	D3DPRESENT_PARAMETERS d3dPresentParameters;
	ZeroMemory(&d3dPresentParameters, sizeof(d3dPresentParameters)); //<-Nollar parametrar
	d3dPresentParameters.Windowed = (!fullscreen); //<-F�nsterl�ge
	d3dPresentParameters.SwapEffect = D3DSWAPEFFECT_COPY; //<-Hur backbuffern ska bytas ut
	d3dPresentParameters.BackBufferFormat = D3DFMT_X8R8G8B8; //<-Backbufferns format (�r os�ker p� den h�r, men f�r lita p� att kurslitteraturen har r�tt!)
	d3dPresentParameters.BackBufferCount = 1; //<-Antal backbuffrar
	d3dPresentParameters.BackBufferHeight = height; //<-h�jd...
	d3dPresentParameters.BackBufferWidth = width; //<--...och bredd p� backbuffern

	//Skapar upp Direct3D-enheten i f�nsterl�ge
	direct3DInterface->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, window, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dPresentParameters, &direct3DDevice);
	if (!direct3DDevice)
		return false;

	//Pekare till backbuffern
	direct3DDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &surfaceBackbuffer);

	return true;
}

void direct3DShutdown(){
	//Avallokerar minne och st�nger ner DX-funktionerna s� att de inte ligger och k�r efter programavslut

	if (direct3DDevice)
		direct3DDevice->Release();

	if (direct3DInterface)
		direct3DInterface->Release();
}

LPDIRECT3DSURFACE9 loadSurface(string filename){

	//Tar in en bild och l�ser in den i en rityta

	LPDIRECT3DSURFACE9 image = NULL;

	
	//Tar fram information om filen(s� som h�jd och bredd)
	D3DXIMAGE_INFO info;
	HRESULT result = D3DXGetImageInfoFromFile(filename.c_str(), &info);
	if (result != D3D_OK)
		return NULL;

	//G�r en surface i samma storlek som bilden
	result = direct3DDevice->CreateOffscreenPlainSurface(info.Width, info.Height, D3DFMT_X8R8G8B8, D3DPOOL_DEFAULT, &image, NULL);
	if (result != D3D_OK)
		return NULL;

	//L�ser in bilden i ritytan och returnerar den
	result = D3DXLoadSurfaceFromFile(image, NULL, NULL, filename.c_str(), NULL, D3DX_DEFAULT, D3DCOLOR_XRGB(0, 0, 0), NULL);
	if (result != D3D_OK)
		return NULL;

	return image;

}

void drawSurface(LPDIRECT3DSURFACE9 dest, float x, float y, LPDIRECT3DSURFACE9 source){

	//Ritar ut en surface p� skr�men

	//Skapar en yta
	D3DSURFACE_DESC desc;
	source->GetDesc(&desc);

	//Skapar rektanglar matchande ytan som ska ritas ut, b�de en k�llrektangel och en m�lrektangel
	RECT sourceRect = { 0, 0, (long)desc.Width, (long)desc.Height };
	RECT destinationRect = { (long)x, (long)y, (long)x + desc.Width, (long)y + desc.Height };

	//Ritar ut ytan p� sk�rmen
	direct3DDevice->StretchRect(source, &sourceRect, dest, &destinationRect, D3DTEXF_NONE);

}

LPDIRECT3DTEXTURE9 loadTexture(string filename, D3DCOLOR transcolor){
	
	//Ladda in bild till texture f�r sprites med hj�lp av DirectX:s egna funktioner
	//Den tar emot filnamn och vilken f�rg som ska vara transparent

	LPDIRECT3DTEXTURE9 texture = NULL;
	
	D3DXIMAGE_INFO info;
	HRESULT result = D3DXGetImageInfoFromFile(filename.c_str(), &info);
	if (result != D3D_OK)
		return NULL;

	//Sedan skapas en textur som kan anv�ndas f�r sprites

	D3DXCreateTextureFromFileEx(direct3DDevice, filename.c_str(), info.Width, info.Height, 1, D3DPOOL_DEFAULT, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, transcolor, &info, NULL, &texture);

	if (result != D3D_OK)
		return NULL;

	return texture;
}