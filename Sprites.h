#ifndef __SPRITES_H_INCLUDED__
#define __SPRITES_H_INCLUDED__

#include "Direct3DGraphics.h"

extern LPD3DXSPRITE pauseSprite;
extern LPDIRECT3DTEXTURE9 pauseTexture;

extern LPD3DXSPRITE levelCompleteSprite;
extern LPDIRECT3DTEXTURE9 levelCompleteTexture;

extern LPD3DXSPRITE gameOverSprite;
extern LPDIRECT3DTEXTURE9 gameOverTexture;

extern LPD3DXSPRITE lifeSprite;
extern LPDIRECT3DTEXTURE9 lifeTexture;

extern LPD3DXSPRITE blueBlockSprite;

extern LPDIRECT3DTEXTURE9 spriteSheet;

extern LPDIRECT3DSURFACE9 topSurface;
extern LPDIRECT3DSURFACE9 splashScreen;

struct SPRITE{
	RECT map; //<-kordinater f�r var i bildfilen spriten finns
	LPD3DXSPRITE sprite; //<-spriten
	LPDIRECT3DTEXTURE9 texture; //<-texturen
	int width, height; //<-h�jd och bredd
	int mapLeft, mapTop; //<-x, y i bildfilen
	int spacing; //<-mellanrum mellan spritens olika animationer i bildfilen
	float x, y; //<-x, y p� sk�rmen
	int frame; //<-animationsruta
};

SPRITE* createSprite(int width, int height, int mapLeft, int mapTop, int spacing, float x, float y, int frame);
void destroySprite(SPRITE* sprite);
bool loadSprites(HWND window);
void drawBlueBlocks(int screenWidth);

void destroyClouds();
void drawClouds();
void moveClouds();


#endif