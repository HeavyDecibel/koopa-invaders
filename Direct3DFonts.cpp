
#include "Direct3DFonts.h"

//De fonter som anv�nds p� olika st�llen i spelet
LPD3DXFONT direct3DMenuFont;
LPD3DXFONT direct3DScoreFont;

void direct3DFontsInit(){
	
	//Initerar fonterna och skapar dom s� att DirectX-enheten kan rita ut dom
	D3DXFONT_DESC menuFont = { 75, 0, 400, 0, false, DEFAULT_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_PITCH, "Script MT Bold" };
	D3DXCreateFontIndirect(direct3DDevice, &menuFont, &direct3DMenuFont);

	D3DXFONT_DESC scoreFont = { 40, 0, 400, 0, false, DEFAULT_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_PITCH, "Stencil" };
	D3DXCreateFontIndirect(direct3DDevice, &scoreFont, &direct3DScoreFont);

}