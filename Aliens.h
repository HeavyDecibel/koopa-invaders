
#ifndef __ALIENS_H_INCLUDED__
#define __ALIENS_H_INCLUDED__

#include "Direct3DGraphics.h"
#include "Sprites.h"

//Enum f�r vilken typ av alien som ska skapas
enum alienPosition{TOP, MIDDLE, BOTTOM};

//ALIEN-struct inneh�llandes sprite, timers, animeringsinformation etc
struct ALIEN{

	int startExplosionTime, explosionDelayTime;

	alienPosition position;
	
	bool destroyed;
	bool explode;
	int explosionFrame;
	int startAnimationTime, animationTimeDelay;

	int destination; //Punkten som alien r�r sig mot.
	SPRITE* sprite;

};

ALIEN* createAlien(alienPosition, int spacing, int destination, HWND window);
void destroyAlien(ALIEN* alien);
void moveAlien(ALIEN* alien, int distance);
void animateAlien(ALIEN* alien);
void drawAlien(ALIEN* alien);

#endif