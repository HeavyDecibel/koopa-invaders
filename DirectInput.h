#ifndef __DIRECTINPUT_H_INCLUDED__
#define __DIRECTINPUT_H_INCLUDED__

#define DIRECTINPUT_VERSION 0x0800

#include <dinput.h>
#include "GameEngine.h"
#include "Direct3DGraphics.h" //<--Den h�r med eftersom direct3DDevice anv�nds

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")


//DirectInput-objekt
extern LPDIRECTINPUT8 directInputInterface; //Gr�nsnittet
extern LPDIRECTINPUTDEVICE8 directInputMouse; //Musenhet (kommer inte anv�ndas i spelet, bara f�r menyn)
extern LPDIRECTINPUTDEVICE8 directInputKeyboard; //Tangentbordsenhet
extern DIMOUSESTATE mouseState; //F�r att returnera musens position, knapptryck etc

//DirectInput-funktioner
//Se DirectInput.cpp f�r kommentarer 
bool directInputInit(HWND);
void directInputUpdate(HWND window);
void directInputShutdown();
int keyDown(int);
bool keyPressed(DWORD key);
int mouseButton(int);
int mouseX();
int mouseY();
POINT mousePosition(HWND window);


#endif