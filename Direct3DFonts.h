
#ifndef __DIRECT3DFONTS_H_INCLUDED__
#define __DIRECT3DFONTS_H_INCLUDED__

#include "Direct3DGraphics.h"

void direct3DFontsInit();	

extern LPD3DXFONT direct3DMenuFont;
extern LPD3DXFONT direct3DScoreFont;

#endif