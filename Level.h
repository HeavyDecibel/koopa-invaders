
#ifndef __LEVEL_H_INCLUDED__
#define __LEVEL_H_INCLUDED__

struct LEVEL{
	
	int levelNumber; //Niv�numret. 
	bool missileShield; //Om aliens missiler stoppar spelarens
	bool alienFire; //Om aliens skjuter tillbaka eller inte
	int alienSpeed; //Hastigheten aliens r�r sig med;
	int alienMissileSpeed; //Hastigheten aliens skott r�r sig med. Ju snabbare dessto sv�rare s�klart
	int noOfAliensMissiles; //Antal missiler som aliens kan ha aktiva p� en och samma g�ng.
	int alienRechargeTime; //Styr hur ofta ett nytt skott genereras f�r aliens

};

LEVEL* createLevel(int levelNumber, int alienSpeed, int alienMissileSpeed, int noOfAliensMissiles, bool missileShield, bool alienFire);
void levelUp(LEVEL* level, int maxMissiles);
void destroyLevel(LEVEL *level);

#endif